import SQLite3
import Foundation

var db: OpaquePointer?
let DB_NAME = "contacts.sqlite"

    func openSQLConnection() -> (message: String, status: String) {
        var message = "SQL Conncection Successfully Opened"
        var status = "Success"

        let fileUrl = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathExtension(DB_NAME)

        if sqlite3_open_v2(fileUrl.path, &db, SQLITE_OPEN_CREATE | SQLITE_OPEN_READWRITE | SQLITE_OPEN_FULLMUTEX, nil) != SQLITE_OK {
            print("SQL Connection Establishing Failure")
            message = "SQL Connection Establishing Failure"
            status = "Failure"
            return (message,status)
        }
        print("filePath = \(fileUrl)")
        return(message,status)
    }

    func deleteDatabase() {
        var fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create:   false).appendingPathComponent(DB_NAME)
        fileURL.deleteLastPathComponent()
    }

    func createTable(query: String) -> (message: String, status: String) {
        var message = "Table Created Successfully"
        var status = "Success"

        if sqlite3_exec(db, query, nil, nil, nil) != SQLITE_OK {
            message = "Table Created Failure"
            status = "Failure"
            return (message,status)
        }

        return (message,status)
    }

    func closeSQLConnection() -> (message: String, status: String) {
        var message = "SQL Connection Is Closed Successfully"
        var status = "Success"

        if sqlite3_close(db) != SQLITE_OK {
            message = "SQL Connection Closing Failure"
            status = "Failure"
            return (message,status)
        }
        return (message,status)
    }

    func insertTableRecords(query: String) -> (message: String, status: String) {
        var message = "Row Inserted Successfully"
        var status = "Success"
        var sqlStatement: OpaquePointer?

        if sqlite3_prepare(db, query, -1, &sqlStatement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("Error In Preparing SQL Insertion Statement : \(errmsg)")
            message = "Error In Preparing SQL Insertion Statement : \(errmsg)"
            status = "Failure"
            return (message,status)
        }

        if sqlite3_step(sqlStatement) != SQLITE_DONE {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("Error Insertin Row In Table : \(errmsg)")
            message = "Error Insertin Row In Table : \(errmsg)"
            return (message,status)
        }

        return (message,status)
    }

    func deleteRowsFromTable(query: String) -> (message: String, status: String) {
        var message = "Row Deleted Successfully"
        var status = "Success"
        var stateMents: OpaquePointer?

        if sqlite3_prepare(db, query, -1, &stateMents, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("Error In Preparing SQL Deletion Statement : \(errmsg)")
            message = "Error In Preparing SQL Deletion Statement : \(errmsg)"
            status = "Failure"
            return (message,status)
        }

        if sqlite3_step(stateMents) != SQLITE_DONE {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("Error Deletion Row In Table : \(errmsg)")
            message = "Error Deletion Row In Table : \(errmsg)"
            return (message,status)
        }
        
        return (message,status)
    }

    func updateRowsInTable(query: String) -> (message: String, status: String) {
        var message = "Row Updated Successfully"
        var status = "Success"
        var stateMents: OpaquePointer?

        if sqlite3_prepare(db, query, -1, &stateMents, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("Error In Preparing SQL Updation Statement : \(errmsg)")
            message = "Error In Preparing SQL Updation Statement : \(errmsg)"
            status = "Failure"
            return (message,status)
        }

        if sqlite3_step(stateMents) != SQLITE_DONE {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("Error Updating Row In Table : \(errmsg)")
            message = "Error Updating Row In Table : \(errmsg)"
            return (message,status)
        }
        
        return (message,status)
    }

    func getRowFromSQLTable(query: String) -> (message: String, status: String, records: [[String : Any]]) {
        var records = [[String : Any]]()
        var message = "All Records Is Successfully Readed"
        var status = "Success"
        var sqlStatement: OpaquePointer?

        if sqlite3_prepare(db, query, -1, &sqlStatement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("Error In Preparing SQL Selection Statement : \(errmsg)")
            message = "Error In Preparing SQL Selection Statement : \(errmsg)"
            status = "Failure"
            return (message,status,records)
        }

        while (sqlite3_step(sqlStatement) == SQLITE_ROW) {
            let id = sqlite3_column_int(sqlStatement, 0)
            let first_name = String(cString: sqlite3_column_text(sqlStatement, 1))
            let last_name = String(cString: sqlite3_column_text(sqlStatement, 2))
            let city = String(cString: sqlite3_column_text(sqlStatement, 3))
            let gender = String(cString: sqlite3_column_text(sqlStatement, 4))
            let phone_number = String(cString: sqlite3_column_text(sqlStatement, 5))

            let studentData = ["id":id,"first_name":first_name,"last_name":last_name,"city":city,"gender":gender,"phone_number":phone_number] as [String : Any]
            records.append(studentData)
        }
    return (message,status,records)
}
