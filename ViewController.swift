//
//  ViewController.swift
//  RECORDS APP
//
//  Created by Mac on 15/03/22.
//

import UIKit

class ViewController: UIViewController {

    var counter = 0
    var timer: Timer? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        createTable()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(onTime(_:)), userInfo: nil, repeats: true)
       
    }

    func createTable() {
        let status = openSQLConnection()
        if status.status == "Success" {
            let query = "CREATE TABLE IF NOT EXISTS RECORDS(_id INTEGER PRIMARY KEY AUTOINCREMENT, FNAME TEXT(10), LNAME TEXT(10), CITY TEXT(10), GENDER TEXT(10), MOBILE INTEGER(10));"
            if userData_app.createTable(query: query).status == "Success" {
                print("Table is created in splash controller screen")
            }
        }
        if closeSQLConnection().status == "Failure" {
            print("connection closure is failure")
        } else {
            print("connection closure is success")
        }
    }
    
    @objc func onTime(_ sender: Timer) {
        
        if (counter == 3) {
            timer?.invalidate()
            onMainSc()
        }
        
        counter = counter + 1
    }
    
    func onMainSc() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "DataScreen") as! DataScreen
        navigationController?.pushViewController(controller, animated: true)
    }

}

