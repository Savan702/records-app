import UIKit

class setRecords: UIViewController,UITextFieldDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        print("set screen is called")
        setUI()
    //    Utils.APP_STATUS = 1
    }
    
    
    func setUI() {
        let btnWidth = 100
        var yPos = 110
        let viewNavigation = UIView()
        viewNavigation.frame = CGRect(x: 0, y: 0, width: Int(Utils.SCREEN_WIDTH), height: yPos)
        viewNavigation.backgroundColor = .systemGray5
        self.view.addSubview(viewNavigation)

        let labelNavigation = UILabel()
        labelNavigation.frame = CGRect(x: 5 * Utils.MARGIN, y: 4 * Utils.MARGIN, width: Int(Utils.SCREEN_WIDTH) - (10 * Utils.MARGIN), height: Int(viewNavigation.bounds.size.height) - (4 * Utils.MARGIN))
        labelNavigation.backgroundColor = .clear
        labelNavigation.textAlignment = .center
        labelNavigation.font = UIFont(name: "Avenir-Medium", size: 25)
        labelNavigation.text = "Records"
        viewNavigation.addSubview(labelNavigation)

        
        let btnBackScreen = createBtn(x: (2 * Utils.MARGIN), y: 5 * Utils.MARGIN, width: 50, height: 50, tag: 301)
        btnBackScreen.setImage(UIImage(systemName: "chevron.left"), for: .normal)
        viewNavigation.addSubview(btnBackScreen)
        btnBackScreen.addTarget(self, action: #selector(onBtnPress(_:)), for: .touchUpInside)

        yPos = yPos + Utils.MARGIN
        
        let viewWidth = Int(Utils.SCREEN_WIDTH)
        let viewHeight = 50

        let LabelNameHead = createLabel(x: 0, y: yPos, width: viewWidth, height: viewHeight, tag: 201, text: "Name")
        self.view.addSubview(LabelNameHead)

        yPos = yPos + viewHeight + Utils.MARGIN
        
        let viewFname = createView(x: Utils.MARGIN, y: yPos, width: (viewWidth / 2) - (2 * Utils.MARGIN), height: viewHeight, tag: 101)
        let txtFieldFname = createTextField(x: Utils.MARGIN / 2, y: (Utils.MARGIN / 2), width: Int((viewFname.bounds.size.width)) - Utils.MARGIN, height: viewHeight - Utils.MARGIN, tag: 401, text: "Enter First Name")
        print("fview size ======= \(viewFname.bounds.size.width)")
        self.view.addSubview(viewFname)
        viewFname.addSubview(txtFieldFname)

        let viewLname = createView(x: viewWidth / 2 + Utils.MARGIN, y: yPos, width: (viewWidth / 2) - 2 * Utils.MARGIN, height: viewHeight, tag: 102)
        let txtFieldLname = createTextField(x: Utils.MARGIN / 2, y: Utils.MARGIN / 2, width: Int((viewLname.bounds.size.width)) - Utils.MARGIN, height: viewHeight - Utils.MARGIN, tag: 402, text: "Enter Last Name")
        self.view.addSubview(viewLname)
        print("Lview size ======= \(viewLname.bounds.size.width)")
        viewLname.addSubview(txtFieldLname)
        
        yPos = yPos + viewHeight + Utils.MARGIN

        let LabelGenderHead = createLabel(x: 0, y: yPos, width: viewWidth, height: viewHeight, tag: 202, text: "Gender")
        self.view.addSubview(LabelGenderHead)

        print("label gender is set in setui")
        
        yPos = yPos + viewHeight + Utils.MARGIN

        let segment = createSegment(x: Utils.MARGIN, y: yPos, width: Int(Utils.SCREEN_WIDTH) - (2 * Utils.MARGIN), height: viewHeight, tag: 301)
        segment.addTarget(self, action: #selector(onSegmentClick(_:)), for: .valueChanged)
        self.view.addSubview(segment)

        yPos = yPos + viewHeight + Utils.MARGIN

        print("segment is set in setui")
        
        let LabelCityHead = createLabel(x: 0, y: yPos, width: viewWidth, height: viewHeight, tag: 203, text: "City")
        self.view.addSubview(LabelCityHead)

        yPos = yPos + viewHeight + Utils.MARGIN

        let viewCity = createView(x: Utils.MARGIN, y: yPos, width: viewWidth - (2 * Utils.MARGIN) , height: viewHeight, tag: 103)
        let txtFieldCity = createTextField(x: Utils.MARGIN / 2, y:Utils.MARGIN / 2, width: Int(viewCity.bounds.size.width) - Utils.MARGIN, height: viewHeight - Utils.MARGIN, tag: 403, text: "Enter City Name")
        self.view.addSubview(viewCity)
        viewCity.addSubview(txtFieldCity)

        yPos = yPos + viewHeight + Utils.MARGIN

        let LabelMobileHead = createLabel(x: 0, y: yPos, width: viewWidth, height: viewHeight, tag: 204, text: "Mobile")
        self.view.addSubview(LabelMobileHead)

        yPos = yPos + viewHeight + Utils.MARGIN
        
        let viewMobile = createView(x: Utils.MARGIN, y: yPos, width: viewWidth - 2 * Utils.MARGIN, height: viewHeight, tag: 104)
        let txtFieldMobile = createTextField(x: Utils.MARGIN / 2, y: Utils.MARGIN / 2, width: Int(viewMobile.bounds.size.width) - Utils.MARGIN, height: viewHeight - Utils.MARGIN, tag: 404, text: "Enter Mobile Number")
        txtFieldMobile.keyboardType = .numberPad
        self.view.addSubview(viewMobile)
        viewMobile.addSubview(txtFieldMobile)

        yPos = yPos + (3 * viewHeight / 2) + Utils.MARGIN

        let btnAdd = createBtn(x: (Int(Utils.SCREEN_WIDTH) / 2) - btnWidth / 2 , y: yPos, width: btnWidth, height: btnWidth / 2, tag: 302)
        btnAdd.backgroundColor = .systemGray5
        btnAdd.setTitleColor(.black, for: .normal)
        btnAdd.setTitle("Add", for: .normal)
        self.view.addSubview(btnAdd)
        btnAdd.addTarget(self, action: #selector(onBtnPress(_:)), for: .touchUpInside)

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        for i in 401...404 {
            let textField = view.viewWithTag(i) as! UITextField
            textField.resignFirstResponder()
        }
        return true
    }
    
    func alertScreen(title: String, msg: String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func ShowAlertmsgWithBakcsc(title: String, msg: String) {
      //  Utils.APP_STATUS = 1
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {_ in self.backSC() }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func createLabel(x: Int, y: Int, width: Int, height: Int, tag: Int, text: String) -> UILabel{
        let label = UILabel()
        label.frame = CGRect(x: x, y: y, width: width, height: height)
        label.tag = tag
        label.textAlignment = .center
        label.text = text
        label.font = UIFont(name: "Avenir", size: 25)
        label.backgroundColor = .clear
        return label
    }

    func createTextField(x: Int, y: Int, width: Int, height: Int, tag: Int, text: String) -> UITextField {
        let textField = UITextField()
        textField.frame = CGRect(x: x, y: y, width: width, height: height)
        textField.tag = tag
        textField.backgroundColor = .clear
        textField.textAlignment = .left
        textField.placeholder = text
        textField.delegate = self
        textField.clearButtonMode = .whileEditing
        textField.font = UIFont(name: "Avenir", size: 20)
        textField.backgroundColor = .systemGray6
        return textField
    }

    func createSegment(x: Int, y: Int, width: Int, height: Int, tag: Int) -> UISegmentedControl {
        let segment = UISegmentedControl()
        segment.frame = CGRect(x: x, y: y, width: width, height: height)
        segment.insertSegment(withTitle: "Male", at: 0, animated: true)
        segment.insertSegment(withTitle: "Female", at: 1, animated: true)
        segment.tag = tag
        segment.selectedSegmentTintColor = .systemGray5
        segment.selectedSegmentIndex = 0
        return segment
    }

    func createView(x: Int, y: Int, width: Int, height: Int, tag: Int) -> UIView {
        let view = UIView()
        view.frame = CGRect(x: x, y: y, width: width, height: height)
        view.tag = tag
        view.backgroundColor = .systemGray6
        return view
    }

    func onAddBtn() {
        let txtFieldFname = view.viewWithTag(401) as! UITextField
        let txtFieldLname = view.viewWithTag(402) as! UITextField
        let txtFieldCity = view.viewWithTag(403) as! UITextField
        let txtFieldMobile = view.viewWithTag(404) as! UITextField

        Utils.SELECTED_CELL_FNAME = txtFieldFname.text ?? ""
        Utils.SELECTED_CELL_LNAME = txtFieldLname.text ?? ""
        Utils.SELECTED_CELL_CITY = txtFieldCity.text ?? ""
        Utils.SELECTED_CELL_MOBILE = txtFieldMobile.text ?? ""

        if (Utils.SELECTED_CELL_FNAME.trimmingCharacters(in: .whitespacesAndNewlines)) == "" {
            alertScreen(title: "Alert", msg: "First Name Should Not Be Nil")
            txtFieldFname.placeholder = "Enter First Name"
            return
        }
        if (Utils.SELECTED_CELL_LNAME.trimmingCharacters(in: .whitespacesAndNewlines)) == "" {
            alertScreen(title: "Alert", msg: "Last Name Should Not Be Nil")
            txtFieldLname.placeholder = "Enter Last Name"
            return
        }
        if (Utils.SELECTED_CELL_CITY.trimmingCharacters(in: .whitespacesAndNewlines)) == "" {
            alertScreen(title: "Alert", msg: "City Name Should Not Be Nil")
            txtFieldCity.placeholder = "Enter City Name"
            return
        }
        if (Utils.SELECTED_CELL_MOBILE.trimmingCharacters(in: .whitespacesAndNewlines)) == "" {
            alertScreen(title: "Alert", msg: "Mobile Number Should Not Be Nil")
            txtFieldMobile.placeholder = "Enter Mobile Number"
            return
        }
        if (Utils.SELECTED_CELL_MOBILE.trimmingCharacters(in: .whitespacesAndNewlines)).count < 10 {
            alertScreen(title: "Alert", msg: "Mobile Number Should Not Be Less Than 10 digit")
            txtFieldMobile.placeholder = "Enter Mobile Number"
            return
        }
        if (Utils.SELECTED_CELL_MOBILE.trimmingCharacters(in: .whitespacesAndNewlines)).count > 10 {
            alertScreen(title: "Alert", msg: "Mobile Number Should Not Be More Than 10 digit")
            txtFieldMobile.placeholder = "Enter Mobile Number"
            return
        }

        let fname = Utils.SELECTED_CELL_FNAME
        let lname = Utils.SELECTED_CELL_LNAME
        let city = Utils.SELECTED_CELL_CITY
        let gender = Utils.SELECTED_CELL_GENDER
        let mobile = Int(Utils.SELECTED_CELL_MOBILE)!
    
        let connectionStatus = openSQLConnection()
        if connectionStatus.status == "Success" {
            let queryInsert = "INSERT INTO RECORDS (FNAME,LNAME,CITY,GENDER,MOBILE) VALUES ('\(fname)','\(lname)','\(city)','\(gender)',\(mobile));"
            print("Insert query \(queryInsert)")
            print(queryInsert)
            let insertStatus = insertTableRecords(query: queryInsert).status
            
            if insertStatus == "Success" {
                print("record successfully inserted")
                print(queryInsert)
                ShowAlertmsgWithBakcsc(title: "Success", msg: "Record Entered Successfully")
            } else {
                print("error in insertion")
            }
        }
        if closeSQLConnection().status == "Failure" {
            print("connection closure is failure")
        } else {
            print("connection closure is success")
        }


    }

    func createBtn(x: Int, y: Int, width: Int, height: Int, tag: Int) -> UIButton {
        let btn = UIButton()
        btn.frame = CGRect(x: x, y: y, width: width, height: height)
        btn.tag = tag
        return btn
    }
    
    func backSC() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func onSegmentClick(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
            case 0: do {
                Utils.SELECTED_CELL_GENDER = "Male"
            }
            case 1: do {
                Utils.SELECTED_CELL_GENDER = "Female"
            }
            default: do {
                print("appropriate segment index")
            }
        }
    }
    
    @objc func onBtnPress(_ sender: UIButton) {
        switch sender.tag {
            case 301: do {
                backSC()
            }
            case 302: do {
                onAddBtn()
            }
            default: do {
            print("select apprepriate btn")
            }
        }
    }
    
}
