//
//  DataScreen.swift
//  RECORDS APP
//
//  Created by Mac on 15/03/22.
//

import UIKit

class DataScreen: UIViewController ,UITableViewDelegate,UITableViewDataSource {

    var table = UITableView()
    var dataArray = [[String : Any]]()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getData()
        table.reloadData()

    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

    }

    override func viewDidLoad() {

        super.viewDidLoad()
        setUI()

        table.delegate = self
        table.dataSource = self
        table.register(cellDefination.self, forCellReuseIdentifier: "TTT")
    }

    func setUI() {
        var yPos = 110
        let viewNavigation = UIView()
        viewNavigation.frame = CGRect(x: 0, y: 0, width: Int(Utils.SCREEN_WIDTH), height: yPos)
        viewNavigation.backgroundColor = .systemGray5
        self.view.addSubview(viewNavigation)

        let labelNavigation = UILabel()
        labelNavigation.frame = CGRect(x: 5 * Utils.MARGIN, y: 4 * Utils.MARGIN, width: Int(Utils.SCREEN_WIDTH) - (10 * Utils.MARGIN), height: Int(viewNavigation.bounds.size.height) - (4 * Utils.MARGIN))
        labelNavigation.backgroundColor = .clear
        labelNavigation.textAlignment = .center
        labelNavigation.font = UIFont(name: "Avenir-Medium", size: 25)
        labelNavigation.text = "Records"
        viewNavigation.addSubview(labelNavigation)

        let btnSetScreen = createBtn(x: Int(Utils.SCREEN_WIDTH) - (Utils.MARGIN * 8), y: 5 * Utils.MARGIN, width: 50, height: 50, tag: 301)
        btnSetScreen.setImage(UIImage(systemName: "plus.rectangle.on.rectangle"), for: .normal)
        viewNavigation.addSubview(btnSetScreen)
        btnSetScreen.addTarget(self, action: #selector(onBtnPress(_:)), for: .touchUpInside)

        table.frame = CGRect(x: 0, y: yPos, width: Int(Utils.SCREEN_WIDTH), height: Int(Utils.SCREEN_HEIGHT) - yPos)
        self.view.addSubview(table)
        yPos = yPos + 10
    }

    func createBtn(x: Int, y: Int, width: Int, height: Int, tag: Int) -> UIButton {
        let btn = UIButton()
        btn.frame = CGRect(x: x, y: y, width: width, height: height)
        btn.tag = tag
        return btn
    }

    func setRecordsSc() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "setRecords") as! setRecords
        navigationController?.pushViewController(controller, animated: true)
    }

    func updateSc() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "UpdateScreen") as! UpdateScreen
        navigationController?.pushViewController(controller, animated: true)
    }

    func getData() {
        dataArray .removeAll()
        let connectionStatus = openSQLConnection()
        if connectionStatus.status == "Success" {
            let queryRead = "SELECT * FROM RECORDS"
            dataArray = getRowFromSQLTable(query: queryRead).records
        }
        if closeSQLConnection().status == "Failure" {
            print("connection closure is failure")
        } else {
            print("connection closure is success")
        }
    }

    func deleteRow(rownumber: Int, id: String) {

        let sqlConnection = openSQLConnection().status

        if sqlConnection == "Success" {
            let querydelete = "DELETE FROM RECORDS WHERE _id = '\(id)';"
            if deleteRowsFromTable(query: querydelete).status == "Success" {
               print("row successfully deleted from table")
                getData()
                table.reloadData()
            }
        }
        _ = closeSQLConnection().status
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell:cellDefination = table.cellForRow(at: indexPath)! as! cellDefination
        let userData = dataArray[indexPath.row]
        Utils.SELECTED_CELL_FNAME = userData["first_name"]! as! String
        Utils.SELECTED_CELL_LNAME = userData["last_name"]! as! String
        Utils.SELECTED_CELL_CITY =  userData["city"]! as! String
        Utils.SELECTED_CELL_GENDER =  userData["gender"]! as! String
        Utils.SELECTED_CELL_MOBILE = userData["phone_number"]! as! String

        cell.btnDelete.isHidden = false
        cell.btnUpdate.isHidden = false
        cell.btnDelete.addTarget(self, action: #selector(onBtnPress(_:)), for: .touchUpInside)
        cell.btnUpdate.addTarget(self, action: #selector(onBtnPress(_:)), for: .touchUpInside)

        let user_id = cell.labelId.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let strArray = user_id.split(separator: ":") as NSArray
        Utils.SELECTED_CELL_U_ID = strArray[1] as! String
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        for i in table.indexPathsForVisibleRows! {
            if i == indexPath {
                let cell:cellDefination = table.cellForRow(at: indexPath)! as! cellDefination
                cell.btnUpdate.isHidden = true
                cell.btnDelete.isHidden = true
            }
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(Utils.CELL_HEIGHT)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let userData = dataArray[indexPath.row] as NSDictionary
        let cell: cellDefination = tableView.dequeueReusableCell(withIdentifier: "TTT", for: indexPath) as! cellDefination

        cell.selectionStyle = .none

        cell.labelName.removeFromSuperview()
        cell.labelCity.removeFromSuperview()
        cell.labelId.removeFromSuperview()
        cell.labelGender.removeFromSuperview()
        cell.labelMobile.removeFromSuperview()
        cell.btnUpdate.removeFromSuperview()
        cell.btnDelete.removeFromSuperview()

        cell.initData()

        cell.labelName.text = " Name: \(userData["first_name"]!) \(userData["last_name"]!)"
        cell.labelGender.text = " Gender: \(userData["gender"]!)"
        cell.labelCity.text = " City: \(userData["city"]!)"
        cell.labelMobile.text = " Mobile: \(userData["phone_number"]!)"
        cell.labelId.text = " Id: \(userData["id"]!)"
        return cell
    }

    @objc func onBtnPress(_ sender: UIButton) {
        switch sender.tag {
            case 301: do {
                setRecordsSc()
            }
            case 302: do {
                updateSc()
            }
            case 303: do {
                deleteRow(rownumber: Utils.SELECTED_CELL, id: Utils.SELECTED_CELL_U_ID)
            }
            default: do {
            print("select apprepriate btn")
            }
        }
    }

}

class cellDefination: UITableViewCell {

    var labelName = UILabel()
    var labelGender = UILabel()
    var labelCity = UILabel()
    var labelMobile = UILabel()
    var labelId = UILabel()

    var btnDelete = UIButton()
    var btnUpdate = UIButton()

    let labelWidth = Int(Utils.SCREEN_WIDTH) - (10 * Utils.MARGIN)
    let labelHieght = 30
    let btnsize = Utils.MARGIN * 4

    func initData() {

        labelName = createLabel(x: 0, y: Utils.MARGIN, width: labelWidth, height: labelHieght, tag: 201)

        labelCity = createLabel(x: 0, y: (3 * labelHieght) + (4 * Utils.MARGIN), width: labelWidth, height: labelHieght, tag: 202)

        labelGender = createLabel(x: (Int(Utils.SCREEN_WIDTH) / 4) + Utils.MARGIN , y: labelHieght + (2 * Utils.MARGIN), width: labelWidth / 2 + ( 2 * Utils.MARGIN), height: labelHieght, tag: 203)

        labelMobile = createLabel(x: 0, y: (2 * labelHieght) + (3 * Utils.MARGIN), width: labelWidth, height: labelHieght, tag: 204)

        labelId = createLabel(x: 0, y: labelHieght + (2 * Utils.MARGIN), width: Int((Utils.SCREEN_WIDTH)), height: labelHieght, tag: 205)

        btnUpdate.frame = CGRect(x: labelWidth + Utils.MARGIN, y: Utils.MARGIN, width: btnsize, height: btnsize)
        btnUpdate.isHidden = true
        btnUpdate.setImage(UIImage(systemName: "pencil"), for: .normal)
        btnUpdate.tag = 302

        btnDelete.frame = CGRect(x: labelWidth + btnsize + (2 * Utils.MARGIN), y: Utils.MARGIN, width: btnsize, height: btnsize)
        btnDelete.isHidden = true
        btnDelete.setImage(UIImage(systemName: "trash.circle.fill"), for: .normal)
        btnDelete.tag = 303

        addSubview(labelName)
        addSubview(labelCity)
        addSubview(labelGender)
        addSubview(labelId)
        addSubview(labelMobile)
        addSubview(btnUpdate)
        addSubview(btnDelete)

    }

    func createLabel(x: Int, y: Int, width: Int, height: Int, tag: Int) -> UILabel{
        let label = UILabel()
        label.frame = CGRect(x: x, y: y, width: width, height: height)
        label.tag = tag
        label.textAlignment = .left
        label.font = UIFont(name: "Avenir", size: 20)
        label.backgroundColor = .clear
        return label
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) not executed")
    }
}
